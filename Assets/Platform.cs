﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Platform : MonoBehaviour {

	public GameObject RealPrefab, CollectiveObj , CollectiveObj2, ColliderCollect;
	public Vector3 Extract, ExtractCollective ;
	public Launcher L;
	public GameObject RealPrefrabClone;
	public int i = 0;
	public  List<GameObject> Waste = new List<GameObject> ();
	public  List<GameObject> CollectiveWaste = new List<GameObject> ();
	public List<GameObject> ColliderWaste = new List<GameObject> ();
	public Text ScoreText;
	int ScoreCount;
	private GameManager G;



	public enum COLOR
	{
		RED,
		GREEN,
		BLUE
	}

	[System.Serializable]

	public class Attributes
	{
		public COLOR color;
		public Material matt;
		public string ColorName;
	}

	public List<Attributes> ColorList = new List<Attributes> ();

	public void init( GameManager Game)
	{
		G = Game;
	}






	// Use this for initialization



	
	// Update is called once per frame
	void Update () {




	}

  public void GenerateInit () {



	RealPrefab = Instantiate (RealPrefab, new Vector3(-17f,-8f,20f), Quaternion.identity) as GameObject;

	Extract = RealPrefab.transform.position;
	RealPrefrabClone = Instantiate (RealPrefab, new Vector3(Extract.x + 20,Extract.y,Extract.z), Quaternion.identity) as GameObject;
	RealPrefrabClone.GetComponent<PlatFormBehave> ().init (this);



	    Waste.Add (RealPrefab);
		Waste.Add (RealPrefrabClone);

		CollectiveObj =	Instantiate (CollectiveObj, new Vector3 (((Waste[1].transform.position.x - Waste[0].transform.position.x)/2 + (Waste[0].transform.position.x )), 1.5f, 20f), Quaternion.identity) as GameObject;
		CollectiveObj.GetComponent<Collective> ().Init (this);
		CollectiveObj2 =	Instantiate (CollectiveObj, new Vector3 (((Waste[1].transform.position.x - Waste[0].transform.position.x)/2 + (Waste[0].transform.position.x )), 4.5f, 20f), Quaternion.identity) as GameObject;
		CollectiveObj2.GetComponent<Collective> ().Init (this);

		CollectiveWaste.Add (CollectiveObj);
		CollectiveWaste.Add (CollectiveObj2);

		ColliderCollect = Instantiate (ColliderCollect, new Vector3 (Waste [0].transform.position.x, Waste [0].transform.position.y - 3.0f, 20f), Quaternion.identity);

		ColliderCollect.GetComponent<ColliderBehave> ().Initial1 (this);
		ColliderWaste.Add (ColliderCollect);

		System.Array A = System.Enum.GetValues (typeof(COLOR));
		COLOR V = (COLOR)A.GetValue(UnityEngine.Random.Range(0,A.Length));
		int index = Random.Range (0, 2);
		if (V == COLOR.RED) {

			RealPrefrabClone.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
			if (index == 0) {
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (1, 3)].matt;
			} else if(index == 1) {
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (1, 3)].matt;
			}
		}

		if (V == COLOR.GREEN) {

			RealPrefrabClone.GetComponent<MeshRenderer> ().material = ColorList [1].matt;

			if (index == 0) {
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [1].matt;
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
			} else if(index == 1) {
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [1].matt;
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
			}
		}

		if (V == COLOR.BLUE) {

			RealPrefrabClone.GetComponent<MeshRenderer> ().material = ColorList [2].matt;

			if (index == 0) {
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (0, 2)].matt;
			} else if(index == 1) {
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (0, 2)].matt;

			}
		}



	L.target = RealPrefrabClone.transform;
}

	public void Generate()
	{
		
		ExtractCollective = CollectiveObj.transform.position;


		GameObject Temp, Temp2, Temp3;





		RealPrefrabClone = Instantiate (RealPrefab, new Vector3(Extract.x + 20,Extract.y,Extract.z), Quaternion.identity) as GameObject;
		RealPrefrabClone.GetComponent<PlatFormBehave> ().init (this);



		Waste.Add (RealPrefrabClone);

		Debug.Log (Waste [1].transform.position);
		Debug.Log (Waste[1].GetComponent<MeshRenderer> ().material);

		CollectiveObj = Instantiate (CollectiveObj, new Vector3 (((Waste[1].transform.position.x - Waste[0].transform.position.x)/2 + (Waste[0].transform.position.x )), ExtractCollective.y, ExtractCollective.z), Quaternion.identity) as GameObject;

		CollectiveObj.GetComponent<Collective> ().Init (this);

		CollectiveObj2 = Instantiate (CollectiveObj, new Vector3 (((Waste[1].transform.position.x - Waste[0].transform.position.x)/2 + (Waste[0].transform.position.x )), ExtractCollective.y + 4.0f, ExtractCollective.z), Quaternion.identity) as GameObject;

		CollectiveObj2.GetComponent<Collective> ().Init (this);


		Debug.Log (CollectiveObj.transform.position);


		CollectiveWaste.Add (CollectiveObj);

		CollectiveWaste.Add (CollectiveObj2);

		ColliderCollect = Instantiate (ColliderCollect, new Vector3 (Waste [0].transform.position.x, Waste [0].transform.position.y - 3.0f, 20f), Quaternion.identity);

		ColliderCollect.GetComponent<ColliderBehave> ().Initial1 (this);
		ColliderWaste.Add (ColliderCollect);

		System.Array A = System.Enum.GetValues (typeof(COLOR));
		COLOR V = (COLOR)A.GetValue(UnityEngine.Random.Range(0,A.Length));
		int index = Random.Range (0, 2);
		if (V == COLOR.RED) {

			RealPrefrabClone.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
			if (index == 0) {
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (1, 2)].matt;
			} else if(index == 1) {
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (1, 2)].matt;
			}
		}

		if (V == COLOR.GREEN) {

			RealPrefrabClone.GetComponent<MeshRenderer> ().material = ColorList [1].matt;

			if (index == 0) {
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [1].matt;
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
			} else if(index == 1) {
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [1].matt;
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
			}
		}

		if (V == COLOR.BLUE) {

			RealPrefrabClone.GetComponent<MeshRenderer> ().material = ColorList [2].matt;

			if (index == 0) {
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (0, 1)].matt;
			} else if(index == 1) {
				CollectiveObj2.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
				CollectiveObj.GetComponent<MeshRenderer> ().material = ColorList [Random.Range (0, 1)].matt;

			}
		}



		Temp = CollectiveWaste [0];

		CollectiveWaste.Remove(CollectiveWaste[0]);

		Temp2 = CollectiveWaste [0];
		CollectiveWaste.Remove (CollectiveWaste [0]);
		Destroy (Temp);
		Destroy (Temp2);

		Temp3 = ColliderWaste [0];

		ColliderWaste.Remove(ColliderWaste[0]);

		Destroy (Temp3);

		ScoreCount++;

		ScoreText.text = ScoreCount.ToString ();

		RealPrefrabClone.name = "Quad";


		L.target = RealPrefrabClone.transform;




	}




}
