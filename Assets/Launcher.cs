﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

	public Rigidbody ball;
	public Transform target;
	public Platform P;
	public float h;
	public float gravity = -10;

	public bool isGrounded = true;
	float startTime = 0f;

	float holdTime = 0.5f;


	// Use this for initialization
	void Start () {
		ball.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		 
		if (Input.GetKeyDown (KeyCode.Space) && isGrounded == true) {

			P.RealPrefrabClone.GetComponent<PlatFormBehave> ().initial (this);
				Debug.Log ("Jump");
				startTime = Time.time;
			 

		}

		if (Input.GetKeyUp (KeyCode.Space) && isGrounded == true ) {

			P.RealPrefrabClone.GetComponent<PlatFormBehave> ().initial (this);
				if (Time.time - startTime >= holdTime) {
					h = 12.0f;
					Debug.Log (Time.time - startTime);
					Launch ();
					isGrounded = false;
				} else {
					h = 8.0f;
					Launch ();
					isGrounded = false;
				}
			}
		

	}



	/*void Update(){
		if (Input.GetKeyDown (KeyCode.Space)) {
			Launch ();
		}
	}*/

	void Launch()
	{
		Physics.gravity = Vector3.up * gravity;
		ball.useGravity = true;
		ball.velocity = CalculateLaunchVelocity ();
	}

	Vector3 CalculateLaunchVelocity()
	{
		float displacementY = target.position.y - ball.position.y;

		Vector3 displacementXZ = new Vector3 (target.position.x - ball.position.x, 0, target.position.z - ball.position.z);
	
		Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
		Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt (-2 * h / gravity) + Mathf.Sqrt (2 * (displacementY - h) / gravity));

		return velocityXZ + velocityY;
	
	}
}
