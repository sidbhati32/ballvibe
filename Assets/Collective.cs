﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Collective : MonoBehaviour {

	private Platform P;

	public void Init(Platform Plat)
	{
		P = Plat;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (this.GetComponent<MeshRenderer> ().sharedMaterial == P.Waste[1].GetComponent<MeshRenderer> ().sharedMaterial)
			Debug.Log ("Continue");
		else
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


	}
}
