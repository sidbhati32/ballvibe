﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plat : MonoBehaviour {

	public GameObject C;
	int check;

	public enum COLOR
	{
		RED,
		GREEN,
		BLUE
	}

	[System.Serializable]

	public class Attributes
	{
		public COLOR color;
		public Material matt;
	}
	public List<Attributes> ColorList = new List<Attributes> ();



	Attributes G;
	// Use this for initialization
	void Start () {

		System.Array A = System.Enum.GetValues (typeof(COLOR));
		COLOR V = (COLOR)A.GetValue(UnityEngine.Random.Range(0,A.Length));

		if (V == COLOR.RED) {

			gameObject.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
			C.GetComponent<MeshRenderer> ().material = ColorList [0].matt;
		}

		if (V == COLOR.GREEN) {

			gameObject.GetComponent<MeshRenderer> ().material = ColorList [1].matt;
			C.GetComponent<MeshRenderer> ().material = ColorList [1].matt;
		}

		if (V == COLOR.BLUE) {

			gameObject.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
			C.GetComponent<MeshRenderer> ().material = ColorList [2].matt;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
